<?php
$customData = [
    'bundle' => 'com.easyrentcars.android',
    'adomain' => 'easyrentcars.com',
    'site_id' => '17136',
    'impid' => '1',
    'bid_id' => '1',
    'group' => 1,
    'seat' => 1,
    'cid' => '1',
    'price' => 0.4,
    'currency' => 'USD',
    'log' => true,
];

$companies = [
    [
        'adid' => 1000,
        'os' => 'ios',
        'geo' => 'USA',
        'cats' => [
            'IAB1',
            'IAB9',
            'IAB20'
        ],
        'banners' => [
            'http://165.227.94.137/img/Rent_0.gif',
            'http://165.227.94.137/img/Rent_1.jpg',
            'http://165.227.94.137/img/Rent_2.jpg'
        ],
        'size' => [
            [
                'w' => 320,
                'h' => 50
            ],
            [
                'w' => 300,
                'h' => 250
            ],
            [
                'w' => 320,
                'h' => 480
            ]
        ]
    ],
    [
        'adid' => 1001,
        'os' => 'android',
        'geo' => 'USA',
        'cats' => [
            'IAB9',
            'IAB13',
            'IAB22',
            'IAB9-23'
        ],
        'banners' => [
            'http://165.227.94.137/img/Rent_3.jpg',
            'http://165.227.94.137/img/Rent_4.jpg',
            'http://165.227.94.137/img/Rent_5.jpg'
        ],
        'size' => [
            [
                'w' => 320,
                'h' => 50
            ],
            [
                'w' => 300,
                'h' => 250
            ],
            [
                'w' => 320,
                'h' => 480
            ]
        ]
    ],
    [
        'adid' => 1002,
        'os' => 'android',
        'geo' => 'IN',
        'cats' => [
            'IAB6',
            'IAB7',
            'IAB10',
            'IAB9-23'
        ],
        'banners' => [
            'http://165.227.94.137/img/Rent_0.jpg',
            'http://165.227.94.137/img/Rent_1.jpg',
            'http://165.227.94.137/img/Rent_2.jpg'
        ],
        'size' => [
            [
                'w' => 320,
                'h' => 50
            ],
            [
                'w' => 300,
                'h' => 250
            ],
            [
                'w' => 320,
                'h' => 480
            ]
        ]
    ]
];
