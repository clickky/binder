<?php
function getData($db)
{
    /**
    WHERE `date`='" . date('Y-m-d') . "'
     */
    $stmt = $db->query('SELECT * FROM `logs`');
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

$data = [];
$message = '';
try {
    $db = new PDO('mysql:host=localhost;dbname=admin_default;charset=utf8mb4', 'admin_default', 'S46T276Q91');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $date = date('Y-m-d');
    /*$db->query("DELETE FROM `logs`");*/
    $data = getData($db);
} catch (Exception $exception) {
    $message = $exception->getMessage();
}

?>


<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script
            src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">

    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

    <script>
        $(document).ready( function () {
            $('#stats').DataTable({
                dom: 'Bfrtip',
                "pageLength": 100,
                buttons: [
                    'csv', 'excel', 'print'
                ]
            });
        } );
    </script>
</head>
<body>
<br /><br /><br />
<div class="container">
    <div class="row">
        <div class="col s12">
            <?php if ($message): ?>
                <div id="card-alert" class="card red">
                    <div class="card-content white-text">
                        <p><?php echo $message ?></p>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            if (count($data)):
                ?>
                <table class="striped centered" id="stats">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Crid</th>
                        <th>Adid</th>
                        <th>Bundle</th>
                        <th>Publisher Id</th>
                        <th>Ip</th>
                        <th>Impressions	</th>
                        <th>Clicks</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($data as $value): ?>
                        <tr>
                            <td><?php echo date('d.m.Y', strtotime($value['date'])); ?></td>
                            <td><?php echo $value['crid']; ?></td>
                            <td><?php echo $value['adid']; ?></td>
                            <td><?php echo $value['bundle']; ?></td>
                            <td><?php echo $value['publisher_id']; ?></td>
                            <td><?php echo $value['ip']; ?>-<?php if($value['ip_click']) echo $value['ip_click']; ?></td>
                            <td><?php echo $value['impressions']; ?></td>
                            <td><?php echo $value['clicks']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>

</div>

</body>
</html>

