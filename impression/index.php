<?php
function getData($db)
{
    $stmt = $db->query("SELECT * FROM `impressions` WHERE `date`='" . date('Y-m-d') . "' LIMIT 1");
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

try {

    $adId = isset($_GET['adId']) ? $_GET['adId'] : '';
    $crid = isset($_GET['crid']) ? $_GET['crid'] : '';
    $bundle = isset($_GET['bundle']) ? $_GET['bundle'] : '';

    $db = new PDO('mysql:host=localhost;dbname=admin_default;charset=utf8mb4', 'admin_default', 'S46T276Q91');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $date = date('Y-m-d');

    $data = getData($db);

    if ($data) {
        $db->exec("UPDATE `impressions` SET `counter`='" . ($data['counter'] + 1) . "' WHERE `date`='" . $date . "'");
    } else {
        $db->exec("INSERT INTO `impressions`(`counter`, `date`) VALUES ('1', '" . $date . "')");
    }

} catch (Exception $exception) {
    echo $exception->getMessage();
}