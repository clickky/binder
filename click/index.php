<?php
$adId = isset($_GET['adId']) ? $_GET['adId'] : '';
$crid = isset($_GET['crid']) ? $_GET['crid'] : '';
$bundle = isset($_GET['bundle']) ? $_GET['bundle'] : '';
$ip = isset($_GET['ip']) ? $_GET['ip'] : '';
$publisher_id = isset($_GET['publisher_id']) ? $_GET['publisher_id'] : '';


$query_data = [
    'adId' => $adId,
    'site_id' => isset($_GET['site_id']) ? $_GET['site_id'] : '',
    'subsite_id' => isset($_GET['subsite_id']) ? $_GET['subsite_id'] : ''
];

$redirect_link = 'https://cpactions.com/api/v1.0/clk/track/proxy?' . http_build_query($query_data);
/*function readLog($type)
{
    return file_get_contents(__DIR__ . '/logs/log_' . $type . '_' . date("d_m_Y") . '.txt');
}

function writeLog($log, $type)
{
    file_put_contents(__DIR__ . '/logs/log_' . $type . '_' . date("d_m_Y") . '.txt', $log);
}*/
function getData($db, $crid, $bundle)
{
    $stmt = $db->query("SELECT * FROM `logs` WHERE `date`='" . date('Y-m-d') . "' AND `crid`='" . $crid . "' AND `bundle`='" . $bundle . "'  LIMIT 1");
    return $stmt->fetch(PDO::FETCH_ASSOC);
}



/**
 * Retrieves the best guess of the client's actual IP address.
 * Takes into account numerous HTTP proxy headers due to variations
 * in how different ISPs handle IP addresses in headers between hops.
 */
function get_ip_address()
{
    // check for shared internet/ISP IP
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    // check for IPs passing through proxies
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        // check if multiple ips exist in var
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (validate_ip($ip)) {
                    return $ip;
                }
            }
        } else {
            if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED'])) {
        return $_SERVER['HTTP_X_FORWARDED'];
    }
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    }
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_FORWARDED_FOR'];
    }
    if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED'])) {
        return $_SERVER['HTTP_FORWARDED'];
    }
    // return unreliable ip since all else failed
    return $_SERVER['REMOTE_ADDR'];
}


$db = new PDO('mysql:host=localhost;dbname=admin_default;charset=utf8mb4', 'admin_default', 'S46T276Q91');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$date = date('Y-m-d');

$data = getData($db, $crid, $bundle);

if ($data) {
    $clicks = (int)$data['clicks'];
    $impressions = (int)$data['impressions'];

    if ($query_data['subsite_id']) {
        $clicks++;
    } else {
        $impressions++;
    }


    $db->exec("UPDATE `logs` SET `impressions`='" . $impressions . "', `clicks`='" . $clicks . "', `ip_click`='".get_ip_address()."' WHERE `date`='" . $date . "' AND `crid`='" . $crid . "' AND `bundle`='" . $bundle . "' AND `ip`='" . $ip . "' AND `publisher_id`='" . $publisher_id . "'");
} else {
    $db->exec("INSERT INTO `logs`(`date`, `crid`, `adid`, `bundle`, `impressions`, `clicks`, `ip`, `publisher_id`)
                          VALUES ('" . $date . "', '" . $crid . "', '" . $adId . "', '" . $bundle . "', '1', '0', '" . $ip . "', '" . $publisher_id . "')");
}

/*$default_data = [
    'crid' => $crid,
    'adid' => $adId,
    'bundle' => $bundle,
    'impressions' => 0,
    'clicks' => 1
];*/

/*if (!$read_data) {
    writeLog(implode('|', $default_data) . PHP_EOL, 'click');
} else {
    $stats = explode("\n", $read_data);
    $write_data = [];
    $has = false;
    foreach ($stats as $stat) {
        $item = explode('|', $stat);
        if (count($item) == 5) {
            //TODO:: more flexible
            if ($item[0] == $crid) {
                $item[4] += 1;
                $has = true;
            }
            $write_data[] = implode('|', $item);
        }

    }
    if (!$has) {
        $write_data[] = implode('|', $default_data);
    }

    writeLog(implode(PHP_EOL, $write_data), 'click');
}*/

if ($query_data['subsite_id']) {
    header('Location: ' . $redirect_link, true, 303);
    exit();
}
