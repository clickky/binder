<?php

$defaultData = [
    'price' => 0.4,
    'currency' => 'USD',
    'link' => 'https://cpactions.com/api/v1.0/clk/track/proxy?adId=155575813&site_id=17136',
    'iurl' => 'http://165.227.94.137/img/320_50.gif',
    'bidid' => '1',
    'cid' => '1',
    'crid' => '1',
    'w' => 320,
    'h' => 50,
    'bundle' => 'com.easyrentcars.android',
    'adid' => '155575813',
    'adomain' => 'easyrentcars.com',
    'impid' => '1',
    'bid_id' => '1',
    'group' => 1,
    'seat' => 1,
    'log' => true
];

function writeLog($log, $type)
{
    file_put_contents(__DIR__ . '/logs/log_' . $type . '_' . date("d_m_Y") . '.txt', $log . PHP_EOL, FILE_APPEND);
}


function json_error_message()
{
    $errors = [
        'JSON_ERROR_NONE' => 'No error',
        'JSON_ERROR_DEPTH' => 'Maximum stack depth exceeded',
        'JSON_ERROR_STATE_MISMATCH' => 'State mismatch (invalid or malformed JSON)',
        'JSON_ERROR_CTRL_CHAR' => 'Control character error, possibly incorrectly encoded',
        'SON_ERROR_SYNTAX' => 'Syntax error',
        'JSON_ERROR_UTF8' => 'Malformed UTF-8 characters, possibly incorrectly encoded'
    ];

    $error = json_last_error();
    return isset($errors[$error]) ? $errors[$error] : 'Unknown error';
}

function check_json_error()
{
    return json_last_error() === JSON_ERROR_NONE;
}

function json_response($message = null, $code = 200)
{
    header_remove();
    http_response_code($code);
    header('Content-Type: application/json');
    header('x-openrtb-version: 2.3');
    header('Status: ' . $code);
    return json_encode($message);
}


$json = file_get_contents('php://input');
/*
$json = '{
	"id": "577D2D31C8F3F201D6A78722E6D59514",
	"device": {
		"js": 1,
		"geo": {
			"country": "USA",
			"lat": 45.6583,
			"lon": -108.3842,
			"type": 2
		},
		"ifa": "a8cb9b32-ca5b-47f0-ac0a-d39032a5a107",
		"carrier": "WIFI",
		"connectiontype": 2,
		"ua": "Mozilla/5.0 (Linux; Android 7.0; SM-G955U Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/63.0.3239.111 Mobile Safari/537.36",
		"dnt": 1,
		"os": "Android",
		"dpidmd5": "3D261CD4EE36154A53347A0FEC85F59A",
		"ip": "174.45.233.137",
		"osv": "7.0",
		"make": "Samsung",
		"devicetype": 4,
		"dpidsha1": "EEF637C775E8EC27CE0E5C15B1759BA84C918A74",
		"model": "SM-G955U"
	},
	"imp": [{
		"displaymanager": "mobfox_sdk",
		"id": "1",
		"bidfloorcur": "USD",
		"displaymanagerver": "Core_3.2.7",
		"secure": 0,
		"instl": 0,
		"ext": {
			"mraid": 2
		},
		"banner": {
			"wmax": 320,
			"btype": [4],
			"id": "1",
			"topframe": 0,
			"pos": 0,
			"hmax": 50,
			"w": 320,
			"battr": [8, 10],
			"h": 50,
			"api": [3, 5]
		},
		"bidfloor": 0.136986
	}],
	"user": {
		"id": "A8CB9B32-CA5B-47F0-AC0A-D39032A5A107"
	},
	"tmax": 211,
	"cur": ["USD"],
	"app": {
		"publisher": {
			"id": "lRFO4CP+HZmvzRl+UhNhdg"
		},
		"cat": ["IAB9-23", "IAB1-2", "IAB1-3", "IAB9-22", "IAB18-1", "IAB9-20"],
		"storeurl": "https://play.google.com/store/apps/details?id=com.animatures.cartoonyourself&hl=en",
		"id": "I5HGUGJB0JzGbHRUKIKMrQ",
		"name": "Cartoon Yourself - emoji caricature selfie camera",
		"domain": "https://play.google.com/store/apps/details?id=com.animatures.cartoonyourself&hl=en",
		"bundle": "com.animatures.cartoonyourself"
	},
	"ext": {
		"udi": {
			"gaid": "a8cb9b32-ca5b-47f0-ac0a-d39032a5a107"
		},
		"fd": 0,
		"utctimestamp": "1516029607708",
		"utcdatetime": "2018-01-15 15:20:07"
	},
	"bcat": ["IAB24", "IAB25", "IAB26"],
	"at": 2
}';*/


try {

    $response = json_decode($json);
    $request = new stdClass();

    if (check_json_error()) {
        $request->cur = $defaultData['currency'];
        $request->id = $response->id;
        $request->bidid = $defaultData['bidid'];

        $bid = new stdClass();
        $bid->iurl = $defaultData['iurl'];
        $bid->cid = $defaultData['cid'];
        $bid->crid = $defaultData['crid'];
        $bid->w = $defaultData['w'];
        $bid->h = $defaultData['h'];
        $bid->price = $defaultData['price'];
        $bid->bundle = $defaultData['bundle'];
        $bid->cat = count($response->app->cat) ? [$response->app->cat[0]] : [];
        $bid->adid = $defaultData['adid'];
        $bid->adomain = [$defaultData['adomain']];
        $bid->adm = '<div><a href="'.$defaultData['link'].'" style="position: absolute; bottom: 0; display: block;width: 100%;z-index: 9999;  text-align: center;"><img src="'.$defaultData['iurl'].'" width="320" height="50"/></a><img src="http://165.227.94.137/impression" width="1" height="1" alt=""></div>';
        $bid->impid = $defaultData['impid'];
        $bid->id = $defaultData['bid_id'];

        $seatbid = new stdClass();
        $seatbid->bid = [$bid];
        $seatbid->group = $defaultData['group'];
        $seatbid->seat = $defaultData['seat'];

        $request->seatbid = [$seatbid];

        /** @noinspection NotOptimalIfConditionsInspection */
        if (($response->imp[0]->bidfloor <= $defaultData['price']) && isset($response->id) && isset($response->imp[0]->bidfloor)) {
            if($defaultData['log']) {
                writeLog('Good - ' . date('d.m.Y H:i:s'), 'Y');
            }
            echo json_response($request, 200);
            exit();
        }
    }
    throw new RuntimeException('no-bid', 204);
} catch (Exception $exception) {
    if($defaultData['log']) {
        writeLog($exception->getMessage() . ' ' . date('d.m.Y H:i:s'), 'N');
    }
    echo json_response([], 204);
    exit();
}
