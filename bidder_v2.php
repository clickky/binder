<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
require_once __DIR__ . '/config/config.php';
require_once __DIR__ . '/config/rules.php';


function writeLog($log, $type)
{
    file_put_contents(__DIR__ . '/logs/log_' . $type . '_' . date("d_m_Y") . '.txt', $log . PHP_EOL, FILE_APPEND);
}


function json_error_message()
{
    $errors = [
        'JSON_ERROR_NONE' => 'No error',
        'JSON_ERROR_DEPTH' => 'Maximum stack depth exceeded',
        'JSON_ERROR_STATE_MISMATCH' => 'State mismatch (invalid or malformed JSON)',
        'JSON_ERROR_CTRL_CHAR' => 'Control character error, possibly incorrectly encoded',
        'SON_ERROR_SYNTAX' => 'Syntax error',
        'JSON_ERROR_UTF8' => 'Malformed UTF-8 characters, possibly incorrectly encoded'
    ];

    $error = json_last_error();
    return isset($errors[$error]) ? $errors[$error] : 'Unknown error';
}

function check_json_error()
{
    return json_last_error() === JSON_ERROR_NONE;
}

function json_response($message = null, $code = 200)
{
    header_remove();
    http_response_code($code);
    header('Content-Type: application/json');
    header('x-openrtb-version: 2.3');
    header('Status: ' . $code);
    return json_encode($message);
}

$json = file_get_contents('php://input');

/*
$json = '{
	"id": "577D2D31C8F3F201D6A78722E6D59514",
	"device": {
		"js": 1,
		"geo": {
			"country": "USA",
			"lat": 45.6583,
			"lon": -108.3842,
			"type": 2
		},
		"ifa": "a8cb9b32-ca5b-47f0-ac0a-d39032a5a107",
		"carrier": "WIFI",
		"connectiontype": 2,
		"ua": "Mozilla/5.0 (Linux; Android 7.0; SM-G955U Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/63.0.3239.111 Mobile Safari/537.36",
		"dnt": 1,
		"os": "Android",
		"dpidmd5": "3D261CD4EE36154A53347A0FEC85F59A",
		"ip": "174.45.233.137",
		"osv": "7.0",
		"make": "Samsung",
		"devicetype": 4,
		"dpidsha1": "EEF637C775E8EC27CE0E5C15B1759BA84C918A74",
		"model": "SM-G955U"
	},
	"imp": [{
		"displaymanager": "mobfox_sdk",
		"id": "1",
		"bidfloorcur": "USD",
		"displaymanagerver": "Core_3.2.7",
		"secure": 0,
		"instl": 0,
		"ext": {
			"mraid": 2
		},
		"banner": {
			"wmax": 320,
			"btype": [4],
			"id": "1",
			"topframe": 0,
			"pos": 0,
			"hmax": 50,
			"w": 320,
			"battr": [8, 10],
			"h": 50,
			"api": [3, 5]
		},
		"bidfloor": 0.136986
	}],
	"user": {
		"id": "A8CB9B32-CA5B-47F0-AC0A-D39032A5A107"
	},
	"tmax": 211,
	"cur": ["USD"],
	"app": {
		"publisher": {
			"id": "lRFO4CP+HZmvzRl+UhNhdg"
		},
		"cat": ["IAB9-23", "IAB1-2", "IAB1-3", "IAB9-22", "IAB18-1", "IAB9-20"],
		"storeurl": "https://play.google.com/store/apps/details?id=com.animatures.cartoonyourself&hl=en",
		"id": "I5HGUGJB0JzGbHRUKIKMrQ",
		"name": "Cartoon Yourself - emoji caricature selfie camera",
		"domain": "https://play.google.com/store/apps/details?id=com.animatures.cartoonyourself&hl=en",
		"bundle": "com.animatures.cartoonyourself"
	},
	"ext": {
		"udi": {
			"gaid": "a8cb9b32-ca5b-47f0-ac0a-d39032a5a107"
		},
		"fd": 0,
		"utctimestamp": "1516029607708",
		"utcdatetime": "2018-01-15 15:20:07"
	},
	"bcat": ["IAB24", "IAB25", "IAB26"],
	"at": 2
}';*/


try {

     $memcached = new Memcached();
     $memcached->addServer('127.0.0.1', 11211);

    $response = json_decode($json);
    $request = new stdClass();

    if (check_json_error()) {

        if (checkRules($rules, $response)) {
            throw new RuntimeException('no-bid', 204);
        }

        $filteredCompanies = findOsArray($companies, $response->device->os, 'os');
        if (!$filteredCompanies) {
            throw new RuntimeException('no-bid', 204);
        }

        $filteredCompanies = findWHArray($filteredCompanies, $response->imp[0]->banner);
        if (!$filteredCompanies) {
            throw new RuntimeException('no-bid', 204);
        }

        $filteredCompanies = findCatArray($filteredCompanies, $response);
        if (!$filteredCompanies) {
            throw new RuntimeException('no-bid', 204);
        }

        $result = $memcached->get('number_'.$response->device->os);
        if ($result === false || $result >= count($filteredCompanies) - 1) {
            $showBanner = 0;
        } else {
            $showBanner = $result + 1;
        }
        $memcached->set('number_'.$response->device->os, $showBanner);

        $company = $filteredCompanies[$showBanner];


        $number = -1;
        foreach ($company['size'] as $k => $v) {
            if ($v['w'] == $response->imp[0]->banner->w && $v['h'] == $response->imp[0]->banner->h) {
                $number = $k;
            }
        }

        if ($number < 0) {
            throw new RuntimeException('no-bid', 204);
        }

        $data = $customData;

        $request->cur = $data['currency'];
        $request->id = $response->id;
        $bidid = $memcached->get('bidid');
        if (!$bidid) {
            $bidid = 1;
        } else {
            $bidid++;
        }
        $request->bidid = (string)md5($bidid);
        $memcached->set('bidid', $bidid);

        $bid = new stdClass();
        $bid->iurl = $company['banners'][$number];
        $bid->cid = $data['cid'];
        $bid->crid = $company['adid'] . '_' . $number;
        $bid->w = $response->imp[0]->banner->w;
        $bid->h = $response->imp[0]->banner->h;
        $bid->price = $data['price'];
        $bid->bundle = $data['bundle'];
        $bid->cat = count($response->app->cat) ? [$response->app->cat[0]] : [];
        $bid->adid = $company['adid'];
        $bid->adomain = [$data['adomain']];

        $impression_link = 'http://165.227.94.137/click?' . http_build_query([
                'adId' => $company['adid'],
                'crid' => $bid->crid,
                'bidid' => $request->bidid,
                'bundle' => $response->app->bundle,
                'ip' => $response->device->ip,
                'publisher_id' => $response->app->publisher->id
            ]);

        $click_link = 'http://165.227.94.137/click?' . http_build_query([
                'adId' => $company['adid'],
                'site_id' => $data['site_id'],
                'subsite_id' => $bid->crid . '_' . $bid->bundle,
                'bidid' => $request->bidid,
                'crid' => $bid->crid,
                'bundle' => $response->app->bundle,
                'ip' => $response->device->ip,
                'publisher_id' => $response->app->publisher->id
            ]);

        $bid->id = $data['bid_id'];
        $bid->adm = '<div id="' . $bid->id . '"><a href="' . $click_link . '" style="position: absolute; bottom: 0; display: block;width: 100%;z-index: 9999;  text-align: center;"><img src="' . $company['banners'][$number] . '" width="320" height="50"/></a><img src="' . $impression_link . '" width="1" height="1" alt=""></div>';
        $bid->impid = $data['impid'];


        $seatbid = new stdClass();
        $seatbid->bid = [$bid];
        $seatbid->group = $data['group'];
        $seatbid->seat = $data['seat'];

        $request->seatbid = [$seatbid];

        /** @noinspection NotOptimalIfConditionsInspection */
        if (($response->imp[0]->bidfloor <= $data['price']) && isset($response->id) && isset($response->imp[0]->bidfloor)) {
            if ($data['log']) {
                writeLog('Good - ' . date('d.m.Y H:i:s'), 'Y');
            }
            echo json_response($request, 200);
            exit();
        }
    }
    throw new RuntimeException('no-bid', 204);
} catch (Exception $exception) {
    if ($customData['log']) {
        writeLog($exception->getMessage() . ' ' . date('d.m.Y H:i:s'), 'N');
    }
    echo json_response([], 204);
    exit();
}

/**
 * @param $array
 * @param $value
 * @param $name
 * @return array
 */
function findOsArray($array, $value, $name)
{
    $result = [];
    foreach ($array as $k => $v) {
        if (strtolower($v[$name]) == strtolower($value)) {
            $result[] = $v;
        }
    }
    return $result;
}

/**
 * @param $array
 * @param $mensure
 * @return array
 */
function findWHArray($array, $mensure)
{
    $result = [];
    foreach ((array)$array as $k => $value) {
        foreach ($value['size'] as $v) {
            if ($v['w'] == $mensure->w && $v['h'] == $mensure->h) {
                $result[] = $value;
            }
        }
    }
    return $result;
}

/**
 * @param $array
 * @param $data
 * @return array
 */
function findCatArray($array, $data)
{
    $result = [];
    foreach ($array as $k => $v) {
        if (count(array_intersect($data->bcat, $v['cats'])) === 0) {
            if (count(array_intersect($data->app->cat, $v['cats'])) > 0) {
                $result[] = $v;
            }
        }

    }
    return $result;
}

/**
 * @param $rules
 * @param $data
 * @return bool
 */
function checkRules($rules, $data)
{
    $publisher_id = $data->app->publisher->id;
    $bundle_id = $data->app->bundle;

    if (count($rules['publisher_bundle']) > 0) {
        foreach ((array)$rules['publisher_bundle'] as $rule) {
            if ($rule['publisher'] == $publisher_id && $rule['bundle'] == $bundle_id) {
                return true;
            }
        }
    }

    if (count($rules['publisher']) > 0) {
        if (in_array($publisher_id, $rules['publisher'], true)) {
            return true;
        }
    }
    if (count($rules['bundle']) > 0) {
        if (in_array($bundle_id, $rules['bundle'], true)) {
            return true;
        }
    }
    return false;
}
