<?php
function getData($db)
{
    /**
    WHERE `date`='" . date('Y-m-d') . "'
     */
    $stmt = $db->query("SELECT * FROM `impressions`");
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

$data = [];
$message = '';
try {
    $db = new PDO('mysql:host=localhost;dbname=admin_default;charset=utf8mb4', 'admin_default', 'S46T276Q91');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $date = date('Y-m-d');
    $data = getData($db);
} catch (Exception $exception) {
    $message = $exception->getMessage();
}

?>


<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col s12">
            <?php if ($message): ?>
                <div id="card-alert" class="card red">
                    <div class="card-content white-text">
                        <p><?php echo $message ?></p>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            if (count($data)):
                ?>
                <table class="striped centered">
                    <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Количество показов</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($data as $value): ?>
                        <tr>
                            <td><?php echo date('d.m.Y', strtotime($value['date'])); ?></td>
                            <td><?php echo $value['counter']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>

</div>

</body>
</html>

